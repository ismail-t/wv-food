module.exports = {
  purge: [
    './src/**/*.html',
    './src/**/*.js',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
    fontFamily : {
      poppins : ['Poppins','sans-seif']
    }
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
